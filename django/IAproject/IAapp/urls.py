from django.conf.urls import url

from . import views


app_name = 'IAapp'

urlpatterns = [
    # ex: /IAapp/
    url(r'^$', views.index, name='index'),
]