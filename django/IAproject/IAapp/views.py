# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template import Context, Template
from django.http import HttpResponse
from django.template import loader
from simpleai.search import SearchProblem, breadth_first, depth_first, uniform_cost, greedy, astar
from simpleai.search.viewers import WebViewer, BaseViewer, ConsoleViewer
from enum import Enum
    
def index(request):
    
   

    def enum(*sequential, **named):
        enums = dict(zip(sequential, range(len(sequential))), **named)
        return type(str('Enum'), (), enums)

    occuped_by = enum('robot', 'aparato', 'robot_aparato', 'salida')


    cool = -150

    aparatos = [(1,2),(2,0),(3,0)]

    exit = (3,3)


    def in_goal(x):
        if x[0] == exit[0] and x[1] == exit[1]:
            return True
        return False


    def getStates(devices):
        states = []
        cc = 0
        for device in devices:
            states.append((device[0], device[1], occuped_by.aparato, 300, cc))
            cc += 1
        states.append((exit[0], exit[1], occuped_by.robot, 0, cc))
        return tuple(states)


    INITIAL = getStates(aparatos)


    def get_robot(state):
        for x in state:
            if x[2] == occuped_by.robot:
                return x
            if x[2] == occuped_by.robot_aparato:
                return x
    def get_random_device(state, row, col):
        devices = []
        for x in state:
            if x[0] == row and x[1] == col:
                devices.append(x)
        return devices[0]

    def tuple2list(t):
        return [list(row) for row in t]


    def list2tuple(t):
        return tuple(tuple(row) for row in t)



    class RobocopProblem(SearchProblem):
        def is_goal(self, state):
            for x in state:
                if (x[2] == occuped_by.aparato):
                    if ((x[0] != exit[0] or x[1] != exit[1]) or (x[3] > 500)):
                        return False
            return True


        def cost(self, state1, action, state2):
            cost = 1
            return cost 



        def actions(self, state):
            available_actions = []

            max_x = exit[0]
            max_y = exit[1]

            robot = get_robot(state)

            for x in state:
                if x[3] > 500:
                    return []

            if robot[2] == occuped_by.robot:
                if robot[0] > 0: #up
                    available_actions.append((1, 0))
                if robot[0] < max_x: #down
                    available_actions.append((2, 0))
                if robot[1] > 0: #left
                    available_actions.append((3, 0))
                if robot[1] < max_y: #right
                    available_actions.append((4, 0))

            for x in state:
                if x[2] == occuped_by.aparato and x[0] == robot[0] and x[1] == robot[1]:
                    available_actions.append((5, 0)) #cool
                    #TODO> ver si las acciones de abajo ya no se habian agregado
                    if robot[0] > 0 and x[3]+25<=500: #up
                        if (1, 1) not in available_actions:
                            available_actions.append((1, 1))
                    if robot[0] < max_x and x[3]+25<=500: #down
                        if (2, 1) not in available_actions:
                            available_actions.append((2, 1))
                    if robot[1] > 0 and x[3]+25<=500: #left
                        if (3, 1) not in available_actions:
                            available_actions.append((3, 1))
                    if robot[1] < max_y and x[3]+25<=500: #right
                        if (4, 1) not in available_actions:
                            available_actions.append((4, 1))

            return available_actions


        def result(self, state, action):
            state = tuple2list(state)

            max_x = exit[0]
            max_y = exit[1]

            robot = get_robot(state)
            device = get_random_device(state, robot[0], robot[1])

            if action[0] == 1:
                state[robot[4]] = [robot[0] - 1, robot[1], occuped_by.robot, 0, robot[4]]
                if action[1] == 1: 
                    state[device[4]] = [device[0] - 1, device[1], occuped_by.aparato, device[3], device[4]]

            if action[0] == 2:
                state[robot[4]] = [robot[0] + 1, robot[1], occuped_by.robot, 0, robot[4]]
                if action[1] == 1:
                    state[device[4]] = [device[0] + 1, device[1], occuped_by.aparato, device[3], device[4]]

            if action[0] == 3:
                state[robot[4]] = [robot[0], robot[1] - 1, occuped_by.robot, 0, robot[4]]
                if action[1] == 1:
                    state[device[4]] = [device[0], device[1] - 1, occuped_by.aparato, device[3], device[4]]


            if action[0] == 4:
                state[robot[4]] = [robot[0], robot[1] + 1, occuped_by.robot, 0, robot[4]]
                if action[1] == 1:
                    state[device[4]] = [device[0], device[1] + 1, occuped_by.aparato, device[3], device[4]]


            if action[0] == 5:
                for x in state:
                    if x[2] == occuped_by.aparato and x[0] == robot[0] and x[1] == robot[1] and not in_goal(x):
                        state[x[4]] = [x[0], x[1], occuped_by.aparato, x[3] + cool, x[4]]

            for s in state:
                if s[2] == occuped_by.aparato and (s[0] != max_x or s[1] != max_y):
                    s[3] += 25
            return list2tuple(state)


        def heuristic(self, state):
            totalheat = 0
            totaldistance = 0

            for x in state:
                if x[2] != occuped_by.robot:
                    totalheat += abs(x[3])
                    totaldistance += abs((exit[0] - x[0]) + (exit[1] - x[1]))
            return totaldistance * 2 ** 2 

    # viewer = WebViewer()
    # viewer = ConsoleViewer()
    viewer = BaseViewer()
    # result = breadth_first(RobocopProblem(INITIAL), graph_search=True)
    # result = depth_first(RobocopProblem(INITIAL), graph_search=True)
    # result = uniform_cost(RobocopProblem(INITIAL), graph_search=True)
    # result = greedy(RobocopProblem(INITIAL), graph_search=True, viewer=viewer)
    result = astar(RobocopProblem(INITIAL), graph_search=True)
    
    actions = []
    
    for action, state in result.path():
        if action != None:
            if action == (1,0):
                actions.append(1)
            if action == (2,0):
                actions.append(2)
            if action == (3,0):
                actions.append(3)
            if action == (4,0):
                actions.append(4)
            if action == (1,1):
                actions.append(5)
            if action == (2,1):
                actions.append(6)
            if action == (3,1):
                actions.append(7)
            if action == (4,1):
                actions.append(8)
            if action == (5,0):
                actions.append(9)
        print('State:', state)

    print('Solution at depth:', len(result.path()))

    print(viewer.stats)
    
    aparatos_list = tuple2list(aparatos)
    
    robot= []
    robot.append(get_robot(INITIAL)[0])
    robot.append(get_robot(INITIAL)[1])


            
            
    data = {
        'aparato1': aparatos_list[0],
        'aparato2': aparatos_list[1],
        'aparato3': aparatos_list[2],
        'robot': robot,
        'actions': actions,
    }  
    
    return render(request, 'index.html', { 'data': data })



















