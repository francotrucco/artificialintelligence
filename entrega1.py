from simpleai.search import SearchProblem, breadth_first, depth_first, \
    greedy, astar
from simpleai.search.viewers import WebViewer, BaseViewer, ConsoleViewer
from enum import Enum
# import numpy as np

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

occuped_by = enum('robot', 'aparato', 'robot_aparato', 'salida')

cool = -150

exit = (3, 3)


def in_goal(x):
    if x[0] == exit[0] and x[1] == exit[1]:
        return True
    return False


def getStates(devices):
    states = []
    cc = 0
    for device in devices:
        states.append((device[0], device[1], occuped_by.aparato, 300, cc))
        cc += 1
    states.append((exit[0], exit[1], occuped_by.robot, 0, cc))
    return tuple(states)


def get_robot(state):
    for x in state:
        if x[2] == occuped_by.robot:
            return x
        if x[2] == occuped_by.robot_aparato:
            return x


# def get_random_device(state, row, col):
#     devices = []
#     for x in state:
#         if x[2] != occuped_by.robot and x[0] == row and x[1] == col:
#             devices.append(x[4])
#     return state[np.random.choice(devices)]

# def get_hottest_device(state, row, col):
#     devices = []
#     for x in state:
#         if x[2] != occuped_by.robot and x[0] == row and x[1] == col:
#             devices.append(x)
#     devices.sort(key=lambda x: x[3], reverse=True)
#     return devices[0]

def tuple2list(t):
    return [list(row) for row in t]


def list2tuple(t):
    return tuple(tuple(row) for row in t)


class RobocopProblem(SearchProblem):

    def is_goal(self, state):
        for x in state:
            if (x[2] == occuped_by.aparato) and (not in_goal(x) or x[3] > 500):
                return False
        return True


    def actions(self, state):
        available_actions = []

        max_x = exit[0]
        max_y = exit[1]

        robot = get_robot(state)

        for x in state:
            if x[3] > 500:
                return available_actions

        if robot[0] > 0:  # up
            available_actions.append(('UP', 0, 0))
        if robot[0] < max_x:  # down
            available_actions.append(('DOWN', 0, 0))
        if robot[1] > 0:  # left
            available_actions.append(('LEFT', 0, 0))
        if robot[1] < max_y:  # right
            available_actions.append(('RIGHT', 0, 0))

        for x in state:
            if x[2] == occuped_by.aparato and x[0] == robot[0] and \
                    x[1] == robot[1]:
                available_actions.append(('COOL', 0, 0))  # cool
                if robot[0] > 0:  # up
                    available_actions.append(('UP', 1, x[4]))
                if robot[0] < max_x:  # down  and x[3]+25 <= 500
                    available_actions.append(('DOWN', 1, x[4]))
                if robot[1] > 0:  # left  and x[3]+25 <= 500
                    available_actions.append(('LEFT', 1, x[4]))
                if robot[1] < max_y:  # right  and x[3]+25 <= 500
                    available_actions.append(('RIGHT', 1, x[4]))

        return available_actions

    def result(self, state, action):
        state = tuple2list(state)

        max_x = exit[0]
        max_y = exit[1]

        robot = get_robot(state)
        if action[1] == 1:
            device = state[action[2]]

        if action[0] == 'UP':
            state[robot[4]] = [robot[0] - 1, robot[1],
                               occuped_by.robot, 0, robot[4]]
            if action[1] == 1:
                state[action[2]] = [device[0] - 1, device[1],
                                    occuped_by.aparato, device[3], device[4]]

        if action[0] == 'DOWN':
            state[robot[4]] = [robot[0] + 1, robot[1],
                               occuped_by.robot, 0, robot[4]]
            if action[1] == 1:
                state[action[2]] = [device[0] + 1, device[1],
                                    occuped_by.aparato, device[3], device[4]]

        if action[0] == 'LEFT':
            state[robot[4]] = [robot[0], robot[1] -
                               1, occuped_by.robot, 0, robot[4]]
            if action[1] == 1:
                state[action[2]] = [device[0], device[1] - 1,
                                    occuped_by.aparato, device[3], device[4]]

        if action[0] == 'RIGHT':
            state[robot[4]] = [robot[0], robot[1] +
                               1, occuped_by.robot, 0, robot[4]]
            if action[1] == 1:
                state[action[2]] = [device[0], device[1] + 1,
                                    occuped_by.aparato, device[3], device[4]]

        if action[0] == 'COOL':
            for x in state:
                if x[2] == occuped_by.aparato and x[0] == robot[0] and \
                        x[1] == robot[1] and not in_goal(x):
                    state[x[4]] = [x[0], x[1],
                                   occuped_by.aparato, x[3] + cool, x[4]]

        for s in state:
            if s[2] == occuped_by.aparato and not in_goal(s):
                s[3] += 25
        return list2tuple(state)

    def heuristic(self, state):
        totaldistance = 0

        for x in state:
            if x[2] == occuped_by.aparato and not in_goal(x):
                totaldistance += (abs(exit[0] - x[0]) + abs(exit[1] - x[1]))
        return totaldistance * 2 ** 2

def resolver(metodo_busqueda, posiciones_aparatos):
    result = None
    viewer = BaseViewer()
    if metodo_busqueda == 'breadth_first':
        result = breadth_first(RobocopProblem(getStates(posiciones_aparatos)),
                               graph_search=True, viewer=viewer)
    elif metodo_busqueda == 'depth_first':
        result = depth_first(RobocopProblem(getStates(posiciones_aparatos)),
                             graph_search=True, viewer=viewer)
    elif metodo_busqueda == 'greedy':
        result = greedy(RobocopProblem(getStates(posiciones_aparatos)),
                        graph_search=True, viewer=viewer)
    elif metodo_busqueda == 'astar':
        result = astar(RobocopProblem(getStates(posiciones_aparatos)),
                       graph_search=True, viewer=viewer)

    print(viewer.stats)
    return result

if __name__ == '__main__':
    resolver()
