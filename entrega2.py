from simpleai.search import (CspProblem, backtrack, min_conflicts,
                             MOST_CONSTRAINED_VARIABLE,
                             LEAST_CONSTRAINING_VALUE,
                             HIGHEST_DEGREE_VARIABLE)
from simpleai.search.viewers import WebViewer, BaseViewer, ConsoleViewer
import itertools


def tuple2list(t):
    return [list(row) for row in t]


def list2tuple(t):
    return tuple(tuple(row) for row in t)

# Variables
elementos = [
    'armadura_roja', 'armadura_verde', 'armadura_azul', 'armadura_amarilla',
    'armadura_blanca', 'amuleto_pendiente_triangulo', 'amuleto_anillo_herrero',
    'amuleto_pulsera_oro', 'amuleto_cinturon_cuero', 'amuleto_moneda_oro',
    'arma_martillo', 'arma_hacha', 'arma_lanza', 'arma_espada', 'arma_garrote',
    'posicion_1', 'posicion_2', 'posicion_3', 'posicion_4', 'posicion_5',
    'escudo_trebol', 'escudo_cruz', 'escudo_mil_pajaros', 'escudo_dragon',
    'escudo_arbol',
]

# Dominio
elementos_asignados = ['Bjarni', '', '', '', '', 'Egil', '', '', '', '',
                       '', '', '', '', 'Cnut', 'Ragnar', '', '', '', '',
                       'Diarf', '', '', '', '']
dom = {}
k = []

for idx, e in enumerate(elementos):
    if elementos_asignados[idx] == '':
        for x in 'Bjarni', 'Egil', 'Cnut', 'Ragnar', 'Diarf':
            if idx < 5:
                if x not in elementos_asignados[0:5]:
                    k.append(x)
            elif idx < 10:
                if x not in elementos_asignados[5:10]:
                    k.append(x)
            elif idx < 15:
                if x not in elementos_asignados[10:15]:
                    k.append(x)
            elif idx < 20:
                if x not in elementos_asignados[15:20]:
                    k.append(x)
            else:
                if x not in elementos_asignados[20:]:
                    k.append(x)
    else:
        k.append(elementos_asignados[idx])
    dom.update({e: k})
    k = []


# Controlando doms
# cc = 1
# for key, values in dom.items():
#     print(key, ' --> ', values)
#     print()
#     if cc % 5 == 0:
#         print()
#     cc += 1


restricc = []


def distintos(variables, values):
    return values[0] != values[1]


def mismo_vikingo(variables, valores):
    return valores[0] == valores[1]


# recibe los dos guerreros a comparar y los guerreros que hay en las 5
# posiciones
def al_lado_vikingo(variables, valores):
    for idx, x in enumerate(valores[2:7]):
        if x == valores[1]:
            if idx == 0:
                return valores[0] == valores[3]
            elif idx == 4:
                return valores[0] == valores[5]
            else:
                return valores[0] == valores[idx+2-1] or \
                    valores[0] == valores[idx+2+1]
    return False

# igual que al_lado_vikingo pero solo se fija a la izquierda


def a_la_izquierda_vikingo(variables, valores):
    for idx, x in enumerate(valores[3:7]):
        if x == valores[1]:
            return valores[0] == valores[idx+2]


# Restricciones fijas asignadas en el DOM
# El color preferido de Bjarni es el rojo, por lo cual su armadura tiene que ser de ese color.
# Egil recuerda que su amuleto era un pendiente con forma de triángulo.
# Diarf está seguro de que su escudo tenía un trébol dibujado.
# Ragnar instintivamente se ubicó en la primer posición, por lo que seguramente es su posición habitual.
# Cnut, conocido por no ser muy delicado, no tiene dudas de que el garrote
# era su arma predilecta.

# Restricciones combinadas

# VESTIMENTA

# El jefe asegura que el guerrero que se vestía de verde, siempre se ubicaba a la izquierda del guerrero de blanco.
# guerrero de verde, guerrero de blanco, guerreros segun posicion
restricc.append(((elementos[1], elementos[4], elementos[15], elementos[
                16], elementos[17], elementos[18], elementos[19]),
    a_la_izquierda_vikingo))

# Y también el guerrero de verde era famoso por su escudo con una cruz
# dibujada.
restricc.append(((elementos[1], elementos[21]), mismo_vikingo))

# El jefe recuerda que el guerrero de amarillo usaba un hacha danesa (un
# hacha de dos manos).
restricc.append(((elementos[3], elementos[11]), mismo_vikingo))


# ARMAS
# Uno de los niños de la aldea recuerda que el guerrero que usaba lanza,
# se ubicaba al lado del guerrero que usaba una brillante pulsera de oro
# para la suerte.
restricc.append(((elementos[12], elementos[7], elementos[15], elementos[
                16], elementos[17], elementos[18], elementos[19]),
    al_lado_vikingo))

# Alguien más sostiene que el guerrero de la lanza siempre estaba al lado
# del guerrero del escudo con dibujo de árbol.
restricc.append(((elementos[12], elementos[24], elementos[15], elementos[
                16], elementos[17], elementos[18], elementos[19]),
    al_lado_vikingo))

# El guerrero que usaba un martillo de guerra, tenía un anillo con un
# dibujo de un herrero para la suerte, todos conocen la historia de cómo
# su padre le regaló ambas cosas.
restricc.append(((elementos[10], elementos[6]), mismo_vikingo))

# El jefe insiste que el guerrero que usaba espada, usaba un escudo
# decorado con un dibujo de un dragón.
restricc.append(((elementos[13], elementos[23]), mismo_vikingo))


# AMULETOS
# Y extrañamente, también recuerda que el guerrero que usaba un cinturón
# de cuero decorado como amuleto, se ubicaba al lado del guerrero del
# hacha danesa.
restricc.append(((elementos[8], elementos[11], elementos[15], elementos[
                16], elementos[17], elementos[18], elementos[19]),
    al_lado_vikingo))


# POSICIONES
# Y que el guerrero del centro (tercera posición) tenía un bello escudo
# decorado con dibujos de mil pájaros diferentes.
restricc.append(((elementos[17], elementos[22]), mismo_vikingo))

# Agnar dice que un guerrero que se ubicaba al lado suyo, usaba armadura azul.
restricc.append(((elementos[16], elementos[2]), mismo_vikingo))

# Sin restriccion
# Y finalmente, hay un quinto amuleto del que nadie recuerda nada: una
# moneda de oro británica.

# No se repite vikingo por tipo de item
for y1, y2 in itertools.combinations(elementos[0:5], 2):
    restricc.append(((y1, y2), distintos))

for y1, y2 in itertools.combinations(elementos[5:10], 2):
    restricc.append(((y1, y2), distintos))

for y1, y2 in itertools.combinations(elementos[10:15], 2):
    restricc.append(((y1, y2), distintos))

for y1, y2 in itertools.combinations(elementos[15:20], 2):
    restricc.append(((y1, y2), distintos))

for y1, y2 in itertools.combinations(elementos[20:25], 2):
    restricc.append(((y1, y2), distintos))


def resolver(metodo_busqueda='backtrack', iteraciones=None):
    problem = CspProblem(elementos, dom, restricc)
    if metodo_busqueda == 'min_conflicts':
        result = min_conflicts(problem, iterations_limit=iteraciones)
    elif metodo_busqueda == 'backtrack':
        result = backtrack(problem, variable_heuristic=HIGHEST_DEGREE_VARIABLE,
                         value_heuristic=LEAST_CONSTRAINING_VALUE)

    return result

if __name__ == '__main__':
    resolver()

    print('backtrack')
    print(resultado)
    print()
    print()
    resultado = min_conflicts(problem, iterations_limit=100)
    print('min-conflicts')
    print(resultado)

# problem = CspProblem(elementos, dom, restricc)
# resultado = backtrack(problem, variable_heuristic=HIGHEST_DEGREE_VARIABLE, value_heuristic=LEAST_CONSTRAINING_VALUE)
# print('backtrack')
# print(repr(resultado))
# print()
# resultado = min_conflicts(problem, iterations_limit = 50)
# print('min-conflicts')
# print(repr(resultado))
